from re import findall,sub
from lxml import html
from time import sleep
from selenium import webdriver
from pprint import pprint
from xvfbwrapper import Xvfb

def parse(url, searchKey):
	response = webdriver.Firefox(executable_path='./geckodriver')
	response.get(url)
	searchKeyElement = response.find_elements_by_xpath('//input[contains(@id,"twotabsearchtextbox")]')
	submitButton = response.find_elements_by_xpath('//input[@type="submit"]')

	if searchKeyElement:
		searchKeyElement[0].send_keys(searchKey)
		submitButton[0].click()
		sleep(8)

		parser = html.fromstring(response.page_source,response.current_url)
		results = parser.xpath('//div[contains(@class, "a-fixed-left-grid-col a-col-right")]')

		for result in results[:5]:
			resultName = result.xpath('.//div/div/a/h2')
			resultName = resultName[0].text_content() if resultName else None
			price = result.xpath('.//div[@class="a-row"]/div/div/a/span[@class="a-offscreen"]')
			price = price[0].text_content().replace("$","").strip() if price else None
			
			item = {
					"itemName":resultName,
					"itemPrice":price,
			}
			pprint(item)


if __name__ == '__main__':
	searchKey = input("Please enter search keyword: ")

	vdisplay = Xvfb()
	vdisplay.start()
	parse("https://www.amazon.com/ref=nav_logo", searchKey)
	vdisplay.stop()