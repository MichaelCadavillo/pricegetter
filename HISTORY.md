# Changelog

## v0.1 - 04/28/2018
 * Getting itemName
 * Getting itemPrice

## v0.2 - 4/28/2018
 * added input search keyword

## v0.3 - 4/28/2018
 * web scraper now works in background

## v0.4 - 5/03/2018
 * final fixes
